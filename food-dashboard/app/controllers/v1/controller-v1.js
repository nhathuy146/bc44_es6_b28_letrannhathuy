export function getValueForm(food) {
  let maMon = document.getElementById("foodID").value;
  let tenMon = document.getElementById("tenMon").value;
  let loai = document.getElementById("loai").value;
  let giaMon = document.getElementById("giaMon").value;
  let khuyenMai = document.getElementById("khuyenMai").value;
  let tinhTrang = document.getElementById("tinhTrang").value;
  let hinhMon = document.getElementById("hinhMon").value;
  let moTa = document.getElementById("moTa").value;
  return { maMon, tenMon, loai, giaMon, khuyenMai, tinhTrang, hinhMon, moTa };
}
