export let renderFoodList = (foodArr) => {
  let contentHTML = "";
  foodArr.forEach((item) => {
    let { maMon, tenMon, loai, khuyenMai, tinhTrang, giaMon } = item;
    let ContentTr = `<tr>
            <td>${maMon}</td>
            <td>${tenMon}</td>
            <td>${loai ? "Chay" : "Mặn"}</td>
            <td>${giaMon}</td>
            <td>${khuyenMai}</td>
            <td>${item.tinhGiaKM()}</td>
            <td>${tinhTrang ? "Còn" : "Hết"}</td>
            <td>
            <button class="btn btn-primary" onclick="suaMonAn(${maMon})">Edit</button>
            <button class="btn btn-danger" onclick="xoaMonAn(${maMon})">Delete</button>
            </td>
        </tr>`;
    contentHTML += ContentTr;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};
