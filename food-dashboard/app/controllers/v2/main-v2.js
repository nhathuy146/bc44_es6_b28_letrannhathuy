import { Food } from "../../models/v1/model.js";
import { getValueForm } from "../v1/controller-v1.js";
import { renderFoodList } from "./controller.js";

const BASE_URL = "https://643a58b8bd3623f1b9b1642a.mockapi.io/food";
let fetchFoodList = () => {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      console.log(`🚀 ~ .then ~ res:`, res);
      let foodArr = res.data.map((item) => {
        let { name, type, discount, img, decs, price, status, id } = item;
        let food = new Food(id, name, type, price, discount, status, img, decs);
        return food;
      });
      renderFoodList(foodArr);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.xoaMonAn = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      fetchFoodList();
    })
    .catch((err) => {
      console.log(err);
    });
};

window.themMon = () => {
  let data = getValueForm();
  let newFood = {
    name: data.tenMon,
    type: data.loai,
    discount: data.khuyenMai,
    img: data.hinhMon,
    decs: data.moTa,
    price: data.giaMon,
    status: data.tinhTrang,
  };
  axios({
    url: BASE_URL,
    method: "POST",
    data: newFood,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");
      fetchFoodList();
    })
    .catch((err) => {});
};

window.suaMonAn = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      $("#exampleModal").modal("show");
      let { id, type, price, img, status, decs, discount, name } = res.data;
      document.getElementById("foodID").value = id;
      document.getElementById("tenMon").value = name;
      document.getElementById("loai").value = type ? "loai1" : "loai2";
      document.getElementById("giaMon").value = price;
      document.getElementById("khuyenMai").value = discount;
      document.getElementById("tinhTrang").value = status ? "1" : "0";
      document.getElementById("hinhMon").value = img;
      document.getElementById("moTa").value = decs;
    })
    .catch((err) => {});
};

window.capNhatMonAn = () => {
  let data = getValueForm();
  let newFood = {
    id: data.maMon,
    name: data.tenMon,
    type: data.loai,
    discount: data.khuyenMai,
    img: data.hinhMon,
    decs: data.moTa,
    price: data.giaMon,
    status: data.tinhTrang,
  };
  console.log(`🚀 ~ newFood:`, newFood);
  axios({
    url: `${BASE_URL}/${data.maMon}`,
    method: "PUT",
    data: newFood,
  })
    .then(function (res) {
      console.log(`🚀 ~ res:`, res);
      fetchFoodList(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
};

fetchFoodList();
